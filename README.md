# SetReviewType plugin for OJS


## About

This [OJS](https://pkp.sfu.ca/ojs/) plugin sets a journal-wide review type.

It applies the default review type to new reviews and prevents later modification of the review type.


## System requirements

This plugin has been tested on OJS version 3.1.1.4.


## Installation

To install:
 - unpack the plugin archive to OJS's plugins/generic directory;
 - enable the plugin by ticking the checkbox for "Set Review Type" in the set of generic plugins available ('Management' > 'Website Settings' > 'Plugins' > 'Generic Plugins').

 Note:
 Make sure the name of the directory for the plugin is 'setReviewType', not 'ojs-set-review-type'.
